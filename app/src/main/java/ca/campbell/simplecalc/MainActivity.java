package ca.campbell.simplecalc;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import java.math.BigDecimal;

//  TODO: add a field to input a 2nd number, get the input and use it in calculations
//  TODO: the inputType attribute forces a number keyboard, don't use it on the second field so you can see the difference

//  TODO: add buttons & methods for subtract, multiply, divide

//  TODO: add input validation: no divide by zero
//  TODO: input validation: set text to show error when it occurs

//  TODO: add a clear button that will clear the result & input fields

//  TODO: the hint for the result widget is hard coded, put it in the strings file

public class MainActivity extends Activity {
    EditText inputNumA;
    EditText inputNumB;
    TextView resultField;
    TextView errorField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inputNumA = (EditText) findViewById(R.id.inputNumA);
        inputNumB = (EditText) findViewById(R.id.inputNumB);

        findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BigDecimal numA = new BigDecimal(inputNumA.getText().toString());
                try {
                    BigDecimal numB = new BigDecimal(inputNumB.getText().toString());
                    displayResult(numA.add(numB));
                }
                catch(NumberFormatException exc) {
                    displayErrors(getString(R.string.divide_zero_err));
                }
            }
        });

        findViewById(R.id.sub).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BigDecimal numA = new BigDecimal(inputNumA.getText().toString());
                try {
                    BigDecimal numB = new BigDecimal(inputNumB.getText().toString());
                    displayResult(numA.subtract(numB));
                }
                catch(NumberFormatException exc) {
                    displayErrors(getString(R.string.divide_zero_err));
                }
            }
        });

        findViewById(R.id.mul).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BigDecimal numA = new BigDecimal(inputNumA.getText().toString());
                try {
                    BigDecimal numB = new BigDecimal(inputNumB.getText().toString());
                    displayResult(numA.multiply(numB));
                }
                catch(NumberFormatException exc) {
                    displayErrors(getString(R.string.divide_zero_err));
                }
            }
        });

        findViewById(R.id.div).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BigDecimal numA = new BigDecimal(inputNumA.getText().toString());
                try {
                    BigDecimal numB = new BigDecimal(inputNumB.getText().toString());

                    if(numB.compareTo(BigDecimal.ZERO) != 0) {
                        displayResult(numA.divide(numB, 5, BigDecimal.ROUND_HALF_UP));
                    }
                    else {
                        displayErrors(getString(R.string.n_a_n_err));
                    }
                }
                catch(NumberFormatException exc) {
                    displayErrors(getString(R.string.divide_zero_err));
                }
            }
        });

        resultField = (TextView) findViewById(R.id.result);
        errorField = (TextView) findViewById(R.id.error);

        findViewById(R.id.clear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inputNumA.setText("");
                inputNumB.setText("");
                clearErrors();
                resultField.setText("");
            }
        });
    }

    public void displayErrors(String... errorList) {
        String errorStr = "";

        for(String error : errorList) {
            errorStr += error + '\n';
        }

        errorField.setText(errorStr);
    }

    public void clearErrors() {
        errorField.setText("");
    }

    public void displayResult(BigDecimal result) {
        clearErrors();
        resultField.setText(String.valueOf(result.floatValue()));
    }

}